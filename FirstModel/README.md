# Generate Data
The data generating script is fairly easy to use.
Only need to tell it the "speed" of the signal, its starting offset, the variance of the noise and the number of datapoints.
E.g.

        $ ./generateData 3.4 0 0.07 512

This will automatically save the results in a directory called `data/Len<length>-Speed<speed>-Offset<offset>-Noise<noise>---<timestamp>` where the respective parameters `<length>`, `<speed>`, etc., are those chosen above.
In it you will find two files:

- `data`: Contains the actual generated data.
- `parameters`: Contains useful parameters for plotting and other possible uses.


# Running the Model
As we the previous codes, the script contained in `computeEstimate.cpp` accept entry in `cin`.
The other parameters are the values $\sigma_U^2$ and $\sigma_Z^2$ of the actual model.
E.g.

        $ ./run 0 0.1 < data/Len512-Speed3.4-Offset0-Noise0.07---1672506646/data

The results are automatically saved in the `data` directory, under an automatically created directory whose name contains the parameter used.


# Plots

The corresponding plotting scripts are

- `InitialData.py`: Plot the initial wrapped data.
![Initial Data](Figure_3.png "Initial Data")

- `Reconstructions.py`: Plot the reconstructions achieved by the model.
![Reconstructions](Figure_1.png "Reconstructions")

- `Components.py`: Plot the three components of the state space model---after we estimated them using the algorithm.
![Components](Figure_2.png "Components")

