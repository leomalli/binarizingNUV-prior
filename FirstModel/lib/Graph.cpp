#include <Eigen/Core>
#include <vector>
#include <cstdio>

#include "BinPrior.hpp"

using Eigen::Matrix;

#define NOTDONE() \
    do { \
        fprintf(stderr, "%s: %s\tis not implemented yet\n", __FILE__, __func__); \
        exit(1); \
    } while(1)


#include "Graph.hpp"

#define VAR_TOL 1e-2
#define DT 1.0

Graph::Graph(const std::vector<double>& data)
    : m_data(data), m_Z_var(1.0), m_sig_U(0.05),
        m_A({   {1, DT, 0},
                {0,  1, 0},
                {0,  0, 1}}),
        m_B1({0,1,0}),
        m_B2({0,0,1}),
        m_C( {1,0,1})
{ m_Initialize(); }

Graph::Graph(const std::vector<double>& data, const double& Z_var, const double& sig_U)
    : m_data(data), m_Z_var(Z_var), m_sig_U(sig_U),
        m_A({   {1, DT, 0},
                {0,  1, 0},
                {0,  0, 1}}),
        m_B1({0,1,0}),
        m_B2({0,0,1}),
        m_C( {1,0,1})
{ m_Initialize(); }

Graph::~Graph()
{}

// Initialize the graph's databins
void Graph::m_Initialize()
{
    // It's not really necessary to save all this data, could be optimized in the future if needed
    m_BNUVs.reserve(m_data.size());
    m_Gs.reserve(m_data.size());
    m_Vp.reserve(m_data.size());
    m_Mp.reserve(m_data.size());
    m_chip.reserve(m_data.size());
    m_Wp.reserve(m_data.size());
    // Populate the BNUV prior array
    for (int i = 0; i < (int)m_data.size(); ++i)
    {
        m_BNUVs.emplace_back(
                std::array<BinPrior, 2>({BinPrior(1.0, 0.0), BinPrior(0.0, -1.0)})
                );
        m_Gs.emplace_back(0.0);
        m_Vp.emplace_back(Matrix<double, 3, 3>::Zero());
        m_Mp.emplace_back(Matrix<double, 3, 1>::Zero());
        m_chip.emplace_back(Matrix<double, 3, 1>::Zero());
        m_Wp.emplace_back(Matrix<double, 3, 3>::Zero());
    }

}

// Run through the graph and populate the F and G matrices
void Graph::ForwardPropagation()
{
    // Formulas
    /* M_k = m_A * ( M_{k-1} + V_{k-1} * m_C.transpose() * G_{k-1} * ( y_{k-1} - C * M_{k-1}) ) + m_B * M_{u_k}*/
    /* V_K = A F_{k-1} V_{k-1} A^T + B V_{u_k} B^T */
    // Method: Compute X_k' message, and then combine with the data's information

    const double initial_var = (m_sig_U == 0) ? 0.1 : m_sig_U;
    // Initial prior values for X_0
    Matrix<double, 3, 3> V{{0.5,0,0},{0,initial_var,0},{0,0,0}};
    /* Matrix<double, 3, 3> V{{1,0,0},{0,1,0},{0,0,1}}; */
    Matrix<double, 3, 1> M{0,0,0};
    // Cov comming from U_k
    const Matrix<double, 3, 3> V_U = m_B1 * m_B1.transpose() * m_sig_U;
    // Multiplication from B2 * B2^T =: BB
    const Matrix<double, 3, 3> BB = m_B2 * m_B2.transpose();
    // Multiplication from C^T * C =: CC
    const Matrix<double, 3, 3> CC = m_C.transpose() * m_C;
    // Stores infos from BNUVs
    Matrix<double, 2, 1> Splus, Sminus;

    // Actual loop
    for (size_t it = 0; it < m_BNUVs.size(); ++it)
    {
        // Get infos from BNUVs
        m_BNUVs[it][0].GetState( &Splus );
        m_BNUVs[it][1].GetState( &Sminus );
        // Compute forward messages
        V = m_A * V * m_A.transpose() + V_U + BB * ( Splus[1] + Sminus[1] );
        m_Vp[it] = V;
        M = m_A * M + m_B2 * ( Splus[0] + Sminus[0] );
        // Save it for later
        m_Mp[it] = M;
        // Define the intermediate value G ( and save it to an array for later use in backward propagation
        m_Gs[it] = 1 / ( m_Z_var + m_C * V * m_C.transpose() );
        // Compute the next values of V_X and M_X
        M = M + V * m_C.transpose() * m_Gs[it] * (m_data[it] - m_C * M);
        V = V - V * m_Gs[it] * CC * V;


    }
}

// Run through the graph backward and propagate messages
//          
//          This assumes that the data has already been propagated
void Graph::BackwardPropagation()
{
    // Multiplication from C^T * C =: CC
    Matrix<double, 3, 3> CC = m_C.transpose() * m_C;
    // Initial values for duals
    Matrix<double, 3, 1> chi {0,0,0};
    Matrix<double, 3, 3> W = Matrix<double, 3, 3>::Zero();

    Matrix<double, 3, 3> F;

    for (int i = (int)m_BNUVs.size(); i > 0; --i)
    {
        F = Matrix<double, 3, 3>::Identity() - m_Vp[i-1] * CC * m_Gs[i-1];
        chi = F.transpose() * chi - m_C.transpose() * m_Gs[i-1] * ( m_data[i-1] - m_C * m_Mp[i-1] );
        // Save it for later use
        m_chip[i-1] = chi;
        // Proper one
        chi = m_A.transpose() * chi;

        W = F.transpose() * W * F + m_Gs[i-1] * CC;
        m_Wp[i-1] = W;
        // Proper one
        W = m_A.transpose() * W * m_A;
    }


}

// Will take the informations in the messages and updates the binarizors accordingly
void Graph::UpdateBin()
{
    Matrix<double, 2, 1> binState;
    double postMean, postVar;
    // Loop through the BNUV and compute their Estimates and update them
    for (int i = 0; i < (int)m_BNUVs.size(); ++i)
    {
        for (int j = 0; j < 2; ++j)
        { 
            m_BNUVs[i][j].GetState( &binState );
            postMean = binState[0] - binState[1] * m_B2.transpose() * m_chip[i];
            postVar = m_B2.transpose() * m_Wp[i] * m_B2;
            postVar = binState[1] - postVar * binState[1] * binState[1];
            m_BNUVs[i][j].UpdateParams( postMean, postVar );
        }
    }
}


// Check is all the BNUV have converged to something or not
bool Graph::HasConverged() const
{
    double cumulative_var = 0.0;
    for (const auto& b : m_BNUVs)
        cumulative_var += b[0].GetVar() + b[1].GetVar();

    return (cumulative_var <= VAR_TOL);
}

void Graph::GetPrediction( std::vector<double>* out ) const
{
    // Reserve space
    out->clear();
    out->reserve( m_data.size() );
    Matrix<double, 3, 1> estimate;
    for (int i = 0; i < (int)m_data.size(); ++i)
    {
        estimate = m_Mp[i] - m_Vp[i] * m_chip[i];
        out->emplace_back( estimate[0] + estimate[2] );
    }
}
void Graph::GetComponent(std::vector<double>* out, const std::size_t& idx) const
{
    out->clear();
    out->reserve( m_data.size() );
    Matrix<double, 3, 1> estimate;
    for (int i = 0; i < (int)m_data.size(); ++i)
    {
        estimate = m_Mp[i] - m_Vp[i] * m_chip[i];
        out->emplace_back( estimate[idx] );
    }
}

int Graph::Fit(const int& n_iter)
{
    int it = 0;
    while ( it < n_iter && !HasConverged() )
    {
        ForwardPropagation();
        BackwardPropagation();
        UpdateBin();
        ++it;
    }
    if (it == n_iter)
        fprintf(stderr, "Warning: (sig_z, sig_u) = (%f, %f). Graph did not converge entirely!\n", m_Z_var, m_sig_U);
    return it;
}

