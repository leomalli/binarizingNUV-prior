#pragma once
#include <Eigen/Core>
#include <array>
#include <cstddef>
#include <vector>
class BinPrior;

class Graph
{
public:
    Graph(const std::vector<double>& data);
    Graph(const std::vector<double>& data, const double& Z_var, const double& sig_U);
    ~Graph();

    void UpdateBin();

    void ForwardPropagation();
    void BackwardPropagation();

    int Fit(const int& n_iter); // Fit the priors to data, return number of iteration

    void GetPrediction( std::vector<double>* out ) const;
    void GetComponent(std::vector<double>* out, const std::size_t& idx) const;

    [[nodiscard]] bool HasConverged() const;

private:
    void m_Initialize();

private:
    // Contains the datapoints
    std::vector<double> m_data;
    // Size of the var of Z
    double m_Z_var, m_sig_U;

    // Parameters, i.e. matrix A, B_1, B_2, and C
    Eigen::Matrix<double, 3, 3> m_A;
    Eigen::Matrix<double, 3, 1> m_B1, m_B2;
    Eigen::Matrix<double, 1, 3> m_C;

    // Contains all the priors in array [S+, S-]
    std::vector<std::array<BinPrior, 2>> m_BNUVs;
    std::vector<double> m_Gs;
    std::vector<Eigen::Matrix<double, 3, 3>> m_Vp;
    std::vector<Eigen::Matrix<double, 3, 1>> m_Mp;
    std::vector<Eigen::Matrix<double, 3, 1>> m_chip;
    std::vector<Eigen::Matrix<double, 3, 3>> m_Wp;
};
