#!/usr/bin/env python3
import numpy as np
import matplotlib.pyplot as plt
import sys
from simpleUtility import get_file_path



def unpack_data(path: str) -> tuple[np.ndarray, np.ndarray]:
    data = np.loadtxt(file_path, dtype=float)
    return data[0,:], data[1:,:]

def generate_plot(X: np.ndarray, Y: np.ndarray) -> None:
    for estimate in Y:
        plt.plot(X, estimate)


if __name__ == '__main__':
    file_path = get_file_path('GenerateFig6.py', '<data.csv>')
    X, Y = unpack_data(file_path)
    generate_plot(X, Y)
    plt.show()


