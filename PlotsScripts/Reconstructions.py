#!/usr/bin/env python3
import sys,  os, re
import numpy as np
import matplotlib.pyplot as plt


def plot_original( data_dir: str ) -> None:
    # Import data and parameters
    params = np.loadtxt( data_dir + '/parameters', dtype=float)
    data = np.mod( np.loadtxt( data_dir + '/data', dtype=float), 1.0)
    X = np.linspace(0, 1, data.shape[0])
    Y = np.mod( X * params[0] + params[1], 1.0)
    plt.plot(X, Y, color='blue', linewidth=1, linestyle=':')
    plt.scatter(X, data, s=3, marker='*', color='red', alpha=0.5)



def plot_reconstructions( directory: str ) -> tuple[float, float]:
    # Load the phi.csv and p_k.csv data
    phi = np.loadtxt(directory + '/phi.csv', dtype=float)
    jumps = np.loadtxt(directory + '/p_k.csv', dtype=float)
    X = np.linspace(0,1, phi.shape[0])

    # Plot result
    result = np.mod( phi + jumps, 1.0 )
    # result = phi + jumps
    plt.plot(X, result, linewidth=1.5, linestyle='-')

    # Extract informations from filenames
    U_pattern = 'u[0-9.]*-'
    Z_pattern = 'z[0-9.]*-'
    U = re.search(U_pattern, directory).group()[1:-1]
    Z = re.search(Z_pattern, directory).group()[1:-1]

    return float(U), float(Z)

if __name__ == '__main__':
    if (len(sys.argv) < 3):
        print('Usage:\n\t./Reconstructions.py <dir_with_initial_data> <dir_with_algorithm_output1> <dir_with_algorithm_output2> ...', file=sys.stderr)
        exit(1)
    data_dir = sys.argv[1]
    results_directories = sys.argv[2:]

    plt.figure(figsize=(20,6))
    plt.title('Sample (wrapped) reconstruction')
    plt.yticks([0,1])
    plot_original( data_dir )
    legend = ['Ground Truth', 'Noisy Data']
    for dirs in results_directories:
        U, Z = plot_reconstructions( dirs )
        legend += [f'$(\\sigma_U, \\sigma_Z) = ({U}, {Z})$']

    plt.legend(legend)
    plt.show()
