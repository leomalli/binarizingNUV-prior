#!/usr/bin/env python3

# Is used to plot the initial data from the first and second models
import matplotlib.pyplot as plt
import numpy as np
from simpleUtility import get_file_path

def get_data(data_dir: str) -> tuple[np.ndarray, np.ndarray]:
    data = np.loadtxt(data_dir + '/data', dtype=float)
    params = np.loadtxt(data_dir + '/parameters', dtype=float)
    return params, data


if __name__ == '__main__':
    # Import given data
    data_dir = get_file_path('InitialData.py', '<data_directory>')
    parameters, data = get_data(data_dir)
    # Wrap data
    data = np.mod(data, 1.0)
    # Create ground truth
    size = data.shape[0]
    X = np.linspace(0, 1, size)
    Y = np.mod(X * parameters[0] + parameters[1], 1.0)

    # Plot everything
    plt.figure(figsize=(11, 5))
    plt.plot(X, Y, color='blue', linewidth=1, linestyle='--')
    plt.plot(X, data, color='red', linewidth=1.2, linestyle='-')
    plt.legend(['Ground truth', 'Input data'])
    plt.show()
