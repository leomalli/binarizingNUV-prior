#!/usr/bin/env python3
import sys
import numpy as np
import matplotlib.pyplot as plt
from simpleUtility import get_file_path

def unpack_data(filename: str) -> tuple[np.ndarray, np.ndarray]:
    return np.loadtxt(filename, dtype=float, max_rows=1), np.loadtxt(filename, dtype=float, skiprows=1)



# Plot the posteriori means
def plot_posteriori(X: np.ndarray) -> None:
    sigZ = X[0,:]
    estimate = X[1,:]

    plt.subplot(211)
    plt.title('Computed posterior mean for different starting values of $\sigma_Z^2$')
    plt.plot(sigZ, estimate)
    plt.xscale('log')
    plt.xlabel('$\sigma_Z^2$')
    plt.ylabel('$\hat{x}$')
    plt.ylim([-0.1,1.1])

# Plot the number of iterations
def plot_iterations(X: np.ndarray) -> None:
    sigZ = X[0,:]
    iterations = X[2,:]

    plt.subplot(212)
    plt.title('Number of Iterations until Convergence')
    plt.yscale('log')
    plt.xscale('log')
    plt.xlabel('$\sigma_Z^2$')
    plt.ylabel('Iterations')
    plt.plot(sigZ, iterations)


if __name__ == '__main__':
    file_path = get_file_path('PhaseEstimator.py', '<data.csv>')
    data, results = unpack_data(file_path)
    print(f'The data consists of:\n{data}')
    plt.figure(figsize=(11,9))
    plt.tight_layout(pad=5.0)
    plot_posteriori(results)
    plot_iterations(results)
    plt.show()

