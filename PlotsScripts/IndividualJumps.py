#!/usr/bin/env python3
import sys, datetime, os, re
import numpy as np
import matplotlib.pyplot as plt

def plot_initial_data(params_path: str) -> None:
    # Load the parameters
    param = np.loadtxt(params_path + "/parameters", dtype=float)
    X = np.linspace(0,1, int(param[-1]))
    Y = X * param[0] + param[1]
    plt.subplot(311)
    plt.plot(X, Y, color='blue', linewidth=1, linestyle='--')

def plot_results( res_dir: str, sig_u: list, sig_z: list, sig_w: list, sig_n: list) -> tuple[list, list, list, list]:
    phi = np.loadtxt( res_dir + "/phi.csv", delimiter=' ', dtype=float)
    noise_jumps = np.loadtxt( res_dir + "/noise_jumps.csv", dtype=float)
    wrap_jumps = np.loadtxt( res_dir + "/wrap_jumps.csv", dtype=float)

    X = np.linspace(0,1, phi.shape[0])
    plt.subplot(311)
    plt.plot(X, phi, linewidth=1)
    plt.subplot(312)
    plt.plot(X, wrap_jumps, linewidth=1)
    plt.subplot(313)
    plt.plot(X, noise_jumps, linewidth=1)

    # Extract the parameters from filename
    U_pattern = 'u[0-9.]*-'
    Z_pattern = 'z[0-9.]*-'
    W_pattern = 'w[0-9.]*-'
    N_pattern = 'n[0-9.]*-'
    U = re.search(U_pattern, res_dir).group()[1:-1]
    Z = re.search(Z_pattern, res_dir).group()[1:-1]
    W = re.search(W_pattern, res_dir).group()[1:-1]
    N = re.search(N_pattern, res_dir).group()[1:-1]
    return sig_u + [float(U)], sig_z + [float(Z)], sig_w + [float(W)], sig_n + [float(N)]




def make_legend(sig_u: list, sig_z: list, sig_w: list, sig_n: list) -> None:

    l = [ f"$\\sigma_U^2={x[0]}, \\sigma_Z^2={x[1]}, \\sigma_W^2={x[2]}, \\sigma_N^2={x[3]}$" for x in zip(sig_u, sig_z, sig_w, sig_n) ]

    plt.subplot(311)
    plt.title('Evolution of $\\varphi_k$')
    plt.legend(['Ground Truth'] + l)
    plt.subplot(312)
    plt.title('Evolution of Wrapping Jumps')
    plt.subplot(313)
    plt.title('Evolution of Noise Jumps')

def save_plot(b: bool, filename: str):
    if (b):
        x = datetime.datetime.now()
        s = str(x)
        plt.savefig(filename + s + ".png", dpi=300)
    else:
        plt.show()


if __name__ == '__main__':
    if (len(sys.argv) < 3):
        print("Usage:\n\tpython3 plotcomponents.py <initial_data_dir> <results_dirs>", file=sys.stderr)
        exit(1)

    params_path = sys.argv[1]
    files_dirs = sys.argv[2:]

    plt.figure(figsize=(20,10))
    plot_initial_data(params_path)

    # Used for the legend
    sig_u = []
    sig_z = []
    sig_w = []
    sig_n = []
    for d in files_dirs:
        sig_u, sig_z, sig_w, sig_n = plot_results(d, sig_u, sig_z, sig_w, sig_n)


    make_legend(sig_u, sig_z, sig_w, sig_n)

    save_plot(False, "figs/SmallZ")
