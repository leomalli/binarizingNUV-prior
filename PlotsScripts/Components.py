#!/usr/bin/env python3
import sys, datetime, os, re
import numpy as np
import matplotlib.pyplot as plt

def plot_initial_data(params_path: str) -> None:
    # Load the parameters
    param = np.loadtxt(params_path + '/parameters', dtype=float)
    X = np.linspace(0,1, int(param[-1]))
    Y = X * param[0] + param[1]
    plt.subplot(311)
    plt.plot(X, Y, color='blue', linewidth=1, linestyle='--')
    plt.subplot(313)
    plt.plot(X, np.ones(shape=X.shape) * param[0] / param[-1], color='blue', linewidth=1, linestyle='--')

def plot_results( res_dir: str, sig_u: list, sig_z: list) -> tuple[list, list]:

    phi = np.loadtxt( res_dir + '/phi.csv', dtype=float)
    jumps = np.loadtxt( res_dir + '/p_k.csv', dtype=float)
    delta = np.loadtxt( res_dir + '/d_phi.csv', dtype=float)

    X = np.linspace(0,1, phi.shape[0])
    plt.subplot(311)
    plt.plot(X, phi, linewidth=1)
    plt.subplot(312)
    plt.plot(X, jumps, linewidth=1)
    plt.subplot(313)
    plt.plot(X, delta, linewidth=1)

    # Extract the parameters from filename
    U_pattern = 'u[0-9.]*-'
    Z_pattern = 'z[0-9.]*-'
    U = re.search(U_pattern, res_dir).group()[1:-1]
    Z = re.search(Z_pattern, res_dir).group()[1:-1]
    return sig_u + [float(U)], sig_z + [float(Z)]




def make_legend(sig_u: list, sig_z: list) -> None:

    l = [ f'$\\sigma_U^2={x[0]}, \\sigma_Z^2={x[1]}$' for x in zip(sig_u, sig_z) ]

    plt.subplot(311)
    plt.title('Evolution of $\\varphi_k$')
    plt.legend(['Ground Truth'] + l)
    plt.subplot(312)
    plt.title('Evolution of $p_k$')
    plt.legend(l)
    plt.subplot(313)
    plt.title('Evolution of $\\Delta\\varphi_k$')
    plt.legend(['Ground Truth'] + l)


if __name__ == '__main__':
    if (len(sys.argv) < 3):
        print('Usage:\n\t./Components.py <initial_data_dir> <result1_dir> <result2_dir> ...', file=sys.stderr)
        exit(1)

    params_path = sys.argv[1]
    files_dirs = sys.argv[2:]

    plt.figure(figsize=(20,10))
    plot_initial_data(params_path)

    # Used for the legend
    sig_u = []
    sig_z = []
    for d in files_dirs:
        sig_u, sig_z = plot_results(d, sig_u, sig_z)


    make_legend(sig_u, sig_z)
    plt.show()
