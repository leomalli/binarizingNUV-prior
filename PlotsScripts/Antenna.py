#!/usr/bin/env python3
import numpy as np
import matplotlib.pyplot as plt
import sys

def get_file_paths() -> list[str]:
    if (len(sys.argv) < 2):
        print("Not enough arguments. Typical usage:\n\t./Antenna.py <data1> [<data2> ...]", file=sys.stderr)
        sys.exit()
    return sys.argv[1:]

def extract_data(files: list[str]) -> list[np.ndarray]:
    data = [ np.loadtxt(file, dtype=float) for file in files ]
    length = len(data[0])
    return data, length




if __name__ == '__main__':
    files = get_file_paths()
    data, length = extract_data(files)
    # We will assume here the data is distributed in [0,20]
    X = np.linspace(0,20, length)
    for Y in data:
        plt.plot(X, Y)

    plt.show()
