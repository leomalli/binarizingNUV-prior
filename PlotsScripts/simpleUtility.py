import sys
def get_file_path(script_name: str, options: str) -> str:
    if (len(sys.argv) < 2):
        print(f"Not enough arguments, usage:\n\
                \t./{script_name} {options}", file=sys.stderr)
        sys.exit()

    return sys.argv[1]

