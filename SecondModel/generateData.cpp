#include <filesystem>
#include <fstream>
#include <random>
#include <ctime>

namespace fs = std::filesystem;

int main(int argc, char *argv[])
{
    // Takes input from argv
    //      a b noise len
    //      f(x) = a*x + b + noise
    //      Generate unwrapped noisy data and saves it
    if (argc != 5)
    {
        fprintf( stderr, "Correct use: <speed> <offset> <noise> <length>\n");
        exit(EXIT_FAILURE);
    }
    // Get variables
    double speed = std::atof(argv[1]), offset = std::atof(argv[2]), noise = std::atof(argv[3]);
    int len = std::atoi(argv[4]);


    // Random components that will be used
    std::random_device rd;
    std::mt19937 mt( rd() );
    std::normal_distribution<double> dist(0.0, std::sqrt(noise));

    // Result
    std::vector<double> data(len);
    for (int k = 0; k < len; ++k)
        data[k] = ( (double)k/(double)(len-1) ) * speed + offset + dist( mt );

    // Generate dataname
    const auto time = std::time(0);
    std::string dstName = "Len" + std::to_string(len) + "-Speed" + std::to_string(speed) + "-Offset" + std::to_string(offset)
        + "-Noise" + std::to_string(noise) + "---" + std::to_string(time);

    // Saves data in data/<datainfos>/files
    fs::create_directories( "data/" + dstName );

    // Params
    std::ofstream fout("data/" + dstName + "/parameters" , std::ios::out | std::ios::trunc);
    fout << speed << ' ' << offset << ' ' << noise << ' ' << len;
    fout.close();

    // Data
    fout.open("data/" + dstName + "/data" , std::ios::out | std::ios::trunc);
    for (auto s{data.size()}; auto x : data)
        fout << x << (--s ? ' ' : '\n');


    fout.close();
    return 0;
}
