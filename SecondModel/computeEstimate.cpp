#include <execution>
#include <vector>
#include <algorithm>
#include <string>
#include <iostream>
#include <filesystem>
#include <cmath>
#include <cstdlib>
#include <stdio.h>

#include "lib/Graph.hpp"
#include "lib/Timer.hpp"
#include "lib/DataLogger.hpp"

namespace fs = std::filesystem;

#define MAX_ITER 1'000'000

int main(int argc, char *argv[])
{

    if (argc < 5)
    {
        fprintf(stderr, "Please provide enough arguments. Typical usage: $ ./run <sig_u> <sig_z> <sig_w> <sig_n> < DATAFILE\n");
        exit(EXIT_FAILURE);
    }
    // Load the data from cin
    std::cout << "Reading the data...\n";
    std::string input;
    std::getline(std::cin, input);
    std::stringstream ss(input);
    std::vector<double> data;
    for (std::string word; ss >> word;)
        data.emplace_back( std::atof(word.c_str()) );

    if (data.empty())
    {
        fprintf(stderr, "Please provide data via cin, i.e. $ ./run < DATA\n");
        exit(EXIT_FAILURE);
    }

    // Get the parameters from argv
    double sig_u = std::atof(argv[1]), sig_z = std::atof(argv[2]);
    double sig_w = std::atof(argv[3]), sig_n = std::atof(argv[4]);

    // Wrap data into [0,1]
    std::transform( std::execution::par_unseq,
            data.begin(), data.end(), data.begin(),
            [](const double& x) -> double {
            return std::fmod( std::fmod(x, 1.0) + 1.0, 1.0);
            });


    Graph g(data, sig_z, sig_u, sig_w, sig_n);
    // Fit the model
    std::cout << "Fitting the model...\n";
    {
        Timer t;
        g.Fit(MAX_ITER);
    }
    if (g.HasConverged())
        std::cout << "The algorithm has converged, i.e. all BNUV are binarized\n";
    else
        std::cout << "The algorithm hasn't converged, i.e. not all BNUV are binarized\n";

    // Extract the results and put them in individual files
    std::vector<double> phi, d_phi, p_k;
    g.GetComponent(&phi, 0);
    g.GetComponent(&d_phi, 1);
    g.GetComponent(&p_k, 2);

    std::vector<double> noise_jump, wrap_jump;
    g.GetJumps(&wrap_jump, &noise_jump);


    // Compose filename
    std::cout << "Writting data to files...\n";
    const auto time = std::time(0);
    std::string dstName = "sig_u" + std::to_string(sig_u) + "-sig_z" + std::to_string(sig_z)
        + "-sig_n" + std::to_string(sig_n) + "-sig_w" + std::to_string(sig_w)
        + "-MaxIter" + std::to_string(MAX_ITER) + "---" + std::to_string(time);

    /* Generate destination directory */
    fs::create_directories( "data/" + dstName );

    logVecDoubleToFile("data/" + dstName + "/phi.csv" , phi);
    logVecDoubleToFile("data/" + dstName + "/d_phi.csv" , d_phi);
    logVecDoubleToFile("data/" + dstName + "/p_k.csv" , p_k);
    logVecDoubleToFile("data/" + dstName + "/noise_jumps.csv" , noise_jump);
    logVecDoubleToFile("data/" + dstName + "/wrap_jumps.csv" , wrap_jump);


    return 0;
}
