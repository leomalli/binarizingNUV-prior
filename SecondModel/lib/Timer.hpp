#pragma once
#include <chrono>
#include <iostream>
// Just a simple bare bone timer class
class Timer
{
public:
    using Clock = std::chrono::high_resolution_clock;
    using Time = std::chrono::time_point<Clock>;
    using ms = std::chrono::milliseconds;

    Time start;
    Timer()
    {
        start = Clock::now();
    }
    ~Timer()
    {
        Time end = Clock::now();
        std::cout << "Took: " << std::chrono::duration_cast<ms>( end - start ).count() << "ms\n";
    }
};

