#include <Eigen/Core>
#include <vector>
#include <cstdio>
#include <iostream>

#define NOTDONE() \
    do { \
        fprintf(stderr, "%s: %s\tis not implemented yet\n", __FILE__, __func__); \
        exit(1); \
    } while(1)

#include "Graph.hpp"
#include "SumBnuv.hpp"

using Eigen::Matrix;
#define VAR_TOL 1e-2
#define DT 1.0
#define MIN_INITIAL_SIG_U 0.1

Graph::Graph(const std::vector<double>& data, const double& sig_Z, const double& sig_U, const double& sig_Wrap, const double& sig_Noise)
    : m_data(data), m_sig_Z(sig_Z), m_sig_U(sig_U), m_sig_Wrap(sig_Wrap), m_sig_Noise(sig_Noise)
{ m_Initialize(); }

Graph::~Graph(){};

// Initialize the graph's databins
void Graph::m_Initialize()
{
    m_A = Matrix<double, 3, 3>({{1, DT, 0},
                                {0,  1, 0},
                                {0,  0, 1}});
    m_B1 = Matrix<double, 3, 1>({0,1,0});
    m_B2 = Matrix<double, 3, 1>({0,0,1});
    m_C  = Matrix<double, 1, 3>({1,0,1});
    // It's not really necessary to save all this data, could be optimized in the future if needed
    m_WrapBnuv.reserve(m_data.size());
    m_NoiseBnuv.reserve(m_data.size());
    m_Gs.reserve(m_data.size());
    m_Vp.reserve(m_data.size());
    m_Mp.reserve(m_data.size());
    m_chip.reserve(m_data.size());
    m_Wp.reserve(m_data.size());
    // Populate the BNUV prior array
    for (int i = 0; i < (int)m_data.size(); ++i)
    {
        m_WrapBnuv.emplace_back( m_sig_Wrap );
        m_NoiseBnuv.emplace_back( m_sig_Noise );
        m_Gs.emplace_back(0.0);
        m_Vp.emplace_back(Matrix<double, 3, 3>::Zero());
        m_Mp.emplace_back(Matrix<double, 3, 1>::Zero());
        m_chip.emplace_back(Matrix<double, 3, 1>::Zero());
        m_Wp.emplace_back(Matrix<double, 3, 3>::Zero());
    }

}

// Run through the graph and populate the F and G matrices
void Graph::m_ForwardPropagation()
{
    // Formulas
    /* M_k = m_A * ( M_{k-1} + V_{k-1} * m_C.transpose() * G_{k-1} * ( y_{k-1} - C * M_{k-1}) ) + m_B * M_{u_k}*/
    /* V_K = A F_{k-1} V_{k-1} A^T + B V_{u_k} B^T */
    // Method: Compute X_k' message, and then combine with the data's information

    const double initial_var = (m_sig_U == 0) ? MIN_INITIAL_SIG_U : m_sig_U;
    // Initial prior values for X_0
    Matrix<double, 3, 3> V{{0,0,0},{0,initial_var,0},{0,0,0}};
    /* Matrix<double, 3, 3> V{{1,0,0},{0,1,0},{0,0,1}}; */
    Matrix<double, 3, 1> M{0,0,0};
    // Cov comming from U_k
    const Matrix<double, 3, 3> V_U = m_B1 * m_B1.transpose() * m_sig_U;
    // Multiplication from B2 * B2^T =: BB
    const Matrix<double, 3, 3> BB = m_B2 * m_B2.transpose();
    // Multiplication from C^T * C =: CC
    const Matrix<double, 3, 3> CC = m_C.transpose() * m_C;

    // Actual loop
    for (size_t it = 0; it < m_NoiseBnuv.size(); ++it)
    {
        // Get infos from BNUVs
        Matrix<double, 2, 1> infoNoise, infoWrap;
        m_NoiseBnuv[it].GetState( &infoNoise );
        m_WrapBnuv[it].GetState( &infoWrap );

        // Compute the messages at the X'_k node
        V = m_A * V * m_A.transpose() + V_U + BB * infoWrap[1];
        m_Vp[it] = V;
        M = m_A * M + m_B2 * infoWrap[0];
        m_Mp[it] = M;

        // Compute the upward messages \backward{m}_{Y_k} and \backward{V}_{Y_k}
        double backward_mean_Y = m_data[it] - infoNoise[0] ;
        double backward_var_Y = infoNoise[1] + m_sig_Z;

        // Define the intermediate value G ( and save it to an array for later use in backward propagation
        m_Gs[it] = 1 / ( backward_var_Y + m_C * V * m_C.transpose() );

        // Compute the next values of V_X and M_X
        M = M + V * m_C.transpose() * m_Gs[it] * (backward_mean_Y - m_C * M);
        V = V - V * m_Gs[it] * CC * V;
    }
}

// Run through the graph backward and propagate messages
//          
//          This assumes that the data has already been propagated
void Graph::m_BackwardPropagation()
{
    // Multiplication from C^T * C =: CC
    const Matrix<double, 3, 3> CC = m_C.transpose() * m_C;
    // Initial values for duals
    Matrix<double, 3, 1> chi {0,0,0};
    Matrix<double, 3, 3> W = Matrix<double, 3, 3>::Zero();
    Matrix<double, 3, 3> F;

    for (int i = (int)m_NoiseBnuv.size(); i > 0; --i)
    {
        // Get infos from noise BNUV
        Matrix<double, 2, 1> infoNoise;
        m_NoiseBnuv[i-1].GetState( &infoNoise );
        double backward_mean_Y = m_data[i-1] - infoNoise[0] ;

        F = Matrix<double, 3, 3>::Identity() - m_Vp[i-1] * CC * m_Gs[i-1];
        chi = F.transpose() * chi - m_C.transpose() * m_Gs[i-1] * ( backward_mean_Y - m_C * m_Mp[i-1] );
        // Save it for later use
        m_chip[i-1] = chi;
        // Proper one
        chi = m_A.transpose() * chi;

        W = F.transpose() * W * F + m_Gs[i-1] * CC;
        m_Wp[i-1] = W;
        // Proper one
        W = m_A.transpose() * W * m_A;
    }


}

// Will take the informations in the messages and updates the binarizors accordingly
void Graph::m_UpdateBin()
{
    for (int i = 0; i < (int)m_NoiseBnuv.size(); ++i)
    {
        // Compute duals for the wrap part, past the matrix multiplication
        double chi = m_B2.transpose() * m_chip[i];
        double W = m_B2.transpose() * m_Wp[i] * m_B2;
        m_WrapBnuv[i].UpdateWithDual(chi, W);

        // Compute duals from the noise part
        // First we have to reconstruct the posterior mean and var before the matrix C
        Matrix<double, 3, 1> post_mean_Y = m_Mp[i] - m_Vp[i] * m_chip[i];
        Matrix<double, 3, 3> post_var_Y = m_Vp[i] - m_Vp[i] * m_Wp[i] * m_Vp[i];

        // Then compute the dual after the matrix multiplication, and update the params with it
        Matrix<double, 2, 1> infos;
        m_NoiseBnuv[i].GetState( &infos );
        double tmp = 1 / ( m_sig_Z + infos[1] );
        chi = tmp * ( m_C * post_mean_Y + infos[0] - m_data[i] );
        W =  m_C * post_var_Y * m_C.transpose();
        W = tmp - tmp * W * tmp;
        m_NoiseBnuv[i].UpdateWithDual(chi, W);
    }
}


// Check is all the BNUV have converged to something or not
bool Graph::HasConverged() const
{
    double cumulative_var = 0.0;
    for (const auto& b : m_NoiseBnuv)
        cumulative_var += b.GetBnuvVar();
    for (const auto& b : m_WrapBnuv)
        cumulative_var += b.GetBnuvVar();

    return (cumulative_var <= VAR_TOL);
}

void Graph::GetPrediction( std::vector<double>* out ) const
{
    // Reserve space
    out->clear();
    out->reserve( m_data.size() );
    Matrix<double, 3, 1> estimate;
    for (int i = 0; i < (int)m_data.size(); ++i)
    {
        estimate = m_Mp[i] - m_Vp[i] * m_chip[i];
        out->emplace_back( estimate[0] + estimate[2] );
    }
}
void Graph::GetComponent(std::vector<double>* out, const std::size_t& idx) const
{
    out->clear();
    out->reserve( m_data.size() );
    Matrix<double, 3, 1> estimate;
    for (int i = 0; i < (int)m_data.size(); ++i)
    {
        estimate = m_Mp[i] - m_Vp[i] * m_chip[i];
        out->emplace_back( estimate[idx] );
    }
}

void Graph::GetJumps(std::vector<double>* out_wrap, std::vector<double>* out_noise) const
{
    out_noise->clear();
    out_wrap->clear();
    out_noise->reserve( m_data.size() );
    out_wrap->reserve( m_data.size() );
    Matrix<double, 2, 1> tmp;
    for (const auto& bnuv : m_NoiseBnuv)
    {
        bnuv.GetState( &tmp );
        out_noise->emplace_back ( tmp[0] );
    }
    for (const auto& bnuv : m_WrapBnuv)
    {
        bnuv.GetState( &tmp );
        out_wrap->emplace_back ( tmp[0] );
    }
}

int Graph::Fit(const int& n_iter)
{
    int it = 0;
    while ( it < n_iter && !HasConverged() )
    {
        m_ForwardPropagation();
        m_BackwardPropagation();
        m_UpdateBin();
        ++it;
    }
    if (it == n_iter)
        fprintf(stderr, "Warning: (sig_z, sig_u) = (%f, %f). Graph did not converge entirely!\n", m_sig_Z, m_sig_U);
    return it;
}
