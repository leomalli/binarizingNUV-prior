# Usage
The usage is similar to the previous model's script.
The difference is that as we add the two parameters $\sigma_W^2$ and $\sigma_N^2$, we must also specify them when running the algorithm, e.g.

        $ ./run 0 0.1 1 100 < data/Len5.../data


# Plotting
The scripts from the previous model are still usable.
The addition of the model is the separation of the BNUV compensation into two distinct one.
In order to see the effects more clearly, we can use `IndividualJumps.py` in order to plot both components separatly.
This script accepts---if need be---multiple directories that could correspond to results from differents starting parameters of the model.
The resulting plot is not always clear to understand and it can be beneficial to plot each result in its own plot.
Nonetheless, the option is there.

- `Reconstruction.py`: Sample to show compatibility with previous scripts.
![Reconstruction](Figure_2.png "Reconstruction")

- `IndividualJumps.py`: Plot the first component of the state space and the two jumps compensations in their respective subplots.
![Jumps](Figure_1.png "Jump Compensations")
