# Generating Data
Use the `generateData.cpp` script in order to generate data.
Two mendatory paramters are the source's location, and the third is the variance of the added noise, e.g.

        $ ./generateData 10 3 0.05

will save the results in the directory `data/` under a name containing the various informations.


# Retreiving the Location
It suffice to feed the generated data into the executable `run`---this comes from the file `estimateDirection.cpp`---via `cin` as with the previous script.
Note that by we use the Second Model's implementation, so you have to choose the parameters $\sigma_U^2$, $\sigma_Z^2$, $\sigma_W^2$, and $\sigma_N^2$.
The script assumes that data is in $[0,2\pi)$ and that the antennas are linearly spaced on $[0,20]$, this can be easily modified.
For instance running

        $ ./run 0.001 0.01 1 10 < data/data.dat

will save the results in the `data` directory, with the parameters chosen as namefile.


# Plotting the results
The only script provided is `Antenna.py`, which only accepts filepaths as arguments.
It also assumes the antennas are linearly spaced in $[0,20]$.
It suffice to run it with filepath(s)---one or more---as arguments, it will simply plot the results in the same panel, as shown in the below figure.
![Antenna Plot](Figure.png "Antenna Plot")
