#include <random>
#include "BinPrior.hpp"

#define EPS 1e-3

BinPrior::BinPrior(const double& a, const double& b)
    : m_a(a), m_b(b), m_sig_a(1.0), m_sig_b(1.0)
{
    static std::random_device rd;
    static std::mt19937 mt( rd() );
    static std::normal_distribution<double> dist( 1.0, 0.05 );

    // Random initialisation
    m_sig_a = dist( mt );
    m_sig_b = dist( mt );
    m_sig_a = (m_sig_a <= EPS) ? 1.0 : m_sig_a;
    m_sig_b = (m_sig_b <= EPS) ? 1.0 : m_sig_b;
}

BinPrior::~BinPrior(){};

void BinPrior::GetState( Eigen::Matrix<double, 2, 1>* out_mat ) const
{
    double var, mean;
    if ( m_sig_a <= EPS || m_sig_b <= EPS )
    {
        var = 0.0;
        mean = (m_sig_a <= EPS) ? m_a : m_b;
    }
    else
    {
        var = m_sig_a * m_sig_b / (m_sig_a + m_sig_b);
        mean = ( m_b * m_sig_a + m_a * m_sig_b ) / (m_sig_a + m_sig_b);
    }
    (*out_mat)[0] = mean;
    (*out_mat)[1] = var;
}

double BinPrior::GetVar() const
{
    if ( m_sig_a <= EPS || m_sig_b <= EPS ) return 0.0;

    return m_sig_a * m_sig_b / (m_sig_a + m_sig_b);
}

void BinPrior::UpdateParams(const double& p_mean, const double& p_var)
{
    m_sig_a = p_var + ( p_mean - m_a ) * ( p_mean - m_a );
    m_sig_b = p_var + ( p_mean - m_b ) * ( p_mean - m_b );
}

