#pragma once
#include <Eigen/Core>
#include "BinPrior.hpp"


// This class represent the sum of two BNUVs (+1, 0) and (-1, 0)
// constrained by an equality node with some gaussian
class SumBnuv
{
public:
    SumBnuv(const double sig);
    ~SumBnuv();

    void GetState( Eigen::Matrix<double, 2, 1>* out_mat ) const;
    double GetBnuvVar() const;

    void UpdateWithDual(const double& chi, const double& W);

private:
    double m_Sig;
    BinPrior m_Plus, m_Minus;
};
