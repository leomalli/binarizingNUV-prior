#pragma once
#include <Eigen/Core>

// This should provide basic method for storing and computing the relevent values of a binarizing prior

class BinPrior
{
public:
    BinPrior(const double& a, const double& b);
    ~BinPrior();

    [[nodiscard]] double GetVar() const;
    void GetState( Eigen::Matrix<double, 2, 1>* out_mat ) const;

    void UpdateParams(const double& p_mean, const double& p_var);

private:
    double m_a, m_b;
    double m_sig_a, m_sig_b;

};
