#pragma once
#include <Eigen/Core>
#include <array>
#include <cstddef>
#include <vector>
class SumBnuv;

class Graph
{
public:
    Graph(const std::vector<double>& data, const double& sig_Z = 0.1, const double& sig_U = 0.05,
                    const double& sig_Wrap = 1, const double& sig_Noise = 1000);
    ~Graph();


    int Fit(const int& n_iter); // Fit the priors to data, return number of iteration

    void GetPrediction( std::vector<double>* out ) const;
    void GetComponent(std::vector<double>* out, const std::size_t& idx) const;
    void GetJumps(std::vector<double>* out_wrap, std::vector<double>* out_noise) const;

    [[nodiscard]] bool HasConverged() const;

private:
    void m_Initialize();

    void m_ForwardPropagation();
    void m_BackwardPropagation();
    void m_UpdateBin();
private:
    // Contains the datapoints
    std::vector<double> m_data;
    // Size of the var of Z
    double m_sig_Z, m_sig_U;
    double m_sig_Wrap, m_sig_Noise;

    // Parameters, i.e. matrix A, B_1, B_2, and C
    Eigen::Matrix<double, 3, 3> m_A;
    Eigen::Matrix<double, 3, 1> m_B1, m_B2;
    Eigen::Matrix<double, 1, 3> m_C;

    // Contains all the small graphs BNUV^- + BNUV^+ constrained by the equality with a centered Gaussian
    std::vector<SumBnuv> m_WrapBnuv;
    std::vector<SumBnuv> m_NoiseBnuv;

    std::vector<double> m_Gs;
    std::vector<Eigen::Matrix<double, 3, 3>> m_Vp;
    std::vector<Eigen::Matrix<double, 3, 1>> m_Mp;
    std::vector<Eigen::Matrix<double, 3, 1>> m_chip;
    std::vector<Eigen::Matrix<double, 3, 3>> m_Wp;
};
