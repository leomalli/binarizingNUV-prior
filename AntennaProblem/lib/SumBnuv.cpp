#include "SumBnuv.hpp"

using Eigen::Matrix;


SumBnuv::SumBnuv(const double sig)
    : m_Sig(sig), m_Plus(1, 0), m_Minus(-1, 0)
{}

SumBnuv::~SumBnuv(){};

void SumBnuv::GetState( Eigen::Matrix<double, 2, 1>* out_mat ) const
{
    // Get the informations from both BNUVs
    Matrix<double, 2, 1> infosPlus, infosMinus;
    m_Plus.GetState( &infosPlus );
    m_Minus.GetState( &infosMinus );

    // Computes the messages and put them in the out matrix
    double sum = infosPlus[1] + infosMinus[1] + m_Sig;
    (*out_mat)[0] = (infosPlus[0] + infosMinus[0]) * m_Sig / sum;
    (*out_mat)[1] = m_Sig * (infosPlus[1] + infosMinus[1]) / sum;
}

void SumBnuv::UpdateWithDual(const double& in_chi, const double& in_W)
{
    // Get through the equality node the quantities \tilde{\chi} and \tilde{W}
    // First we need states of current binarizors
    Matrix<double, 2, 1> infosPlus, infosMinus;
    m_Plus.GetState( &infosPlus );
    m_Minus.GetState( &infosMinus );
    double M = infosPlus[0] + infosMinus[0];
    double V = infosPlus[1] + infosMinus[1];
    double tmpFactor = 1 / ( m_Sig + V );
    double chi = (1 - V * tmpFactor) * in_chi + tmpFactor * M;
    double W = ( 1 - V * tmpFactor ) * ( 1 - V * tmpFactor ) * in_W + tmpFactor;

    // Use the duals to compute posteriors for both BNUVs and update them
    double post_mean = infosPlus[0] - infosPlus[1] * chi;
    double post_var = infosPlus[1] - infosPlus[1] * infosPlus[1] * W;
    m_Plus.UpdateParams( post_mean, post_var );

    post_mean = infosMinus[0] - infosMinus[1] * chi;
    post_var = infosMinus[1] - infosMinus[1] * infosMinus[1] * W;
    m_Minus.UpdateParams( post_mean, post_var );
}

double SumBnuv::GetBnuvVar() const
{ return m_Plus.GetVar() + m_Minus.GetVar(); }
