#include <Eigen/Core>
#include <Eigen/Dense>
#include <iostream>

using Eigen::Matrix;
using Eigen::Dynamic;

#define TOL 1e-4

void computeRes(Matrix<double, Dynamic, 1>& residues, Matrix<double, 3, 1>& params, const Matrix<double, Dynamic, 2>& data)
{
    /* Defines the objective func */
    auto func = [&params](const auto& x) -> auto {
        return  params[2] - std::sqrt( (params[0] - x)*(params[0] - x) + std::exp(2.0*params[1]) );
    };
    /* Compute each residues */
    for (int i = 0; i < (int)data.rows(); ++i)
        residues(i) = data(i, 0) - func( data(i, 1) );

}

void computeJac(Matrix<double, Dynamic, 3>& jacobian, Matrix<double, 3, 1>& params, const Matrix<double, Dynamic, 2>& data)
{
    auto d1 = [&params](const auto& x) -> auto {
        return (params[0] - x) / std::sqrt( (params[0] - x)*(params[0] - x) + std::exp(2.0*params[1]) );
    };
    auto d2 = [&params](const auto& x) -> auto {
        return std::exp(2.0*params[1])  / std::sqrt( (params[0] - x)*(params[0] - x) + std::exp(2.0*params[1]) );
    };
    /* auto d3 = [](const auto&) -> auto { */
    /*     return 1; */
    /* }; */

    for (int i = 0; i < (int)data.rows(); ++i)
    {
        jacobian(i, 0) = d1(data(i, 1));
        jacobian(i, 1) = d2(data(i, 1));
        jacobian(i, 2) = 1;
    }

}

void GetSourceLocation(const int& start_x, const int& end_x, const std::vector<double>& unwrapped_phase, const int& max_it = 1000, const double& lr = 0.1)
{
    /* Construct the data */
    Matrix<double, Dynamic, 2> data(unwrapped_phase.size(), 2);
    for (int i = 0; i < (int)unwrapped_phase.size(); ++i)
    {
        data(i, 1) = unwrapped_phase[i];
        data(i, 0) = (double)(i) / (double)(unwrapped_phase.size()-1) * (double)(end_x - start_x);
    }
    /* The initial parameters */
    Matrix<double, 3, 1> lambda = {1,1,1};
    /* The residuals */
    Matrix<double, Dynamic, 1> R(unwrapped_phase.size(), 1);
    /* The Jacobian */
    Matrix<double, Dynamic, 3> J(unwrapped_phase.size(), 3);
    for (int i = 0; i < max_it; ++i)
    {
        computeRes(R, lambda, data);
        computeJac(J, lambda, data);
        Matrix<double, 3, 1> A = (J.transpose() * J).ldlt().solve(J.transpose() * R);
        std::cout << A << '\n';
        lambda -= lr * A;
    }

    std::cout << lambda[0] << ' ' << std::exp(lambda[1]) << '\n';
}
