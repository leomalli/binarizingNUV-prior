#include <iostream>
#include <filesystem>
#include <cmath>
#include <algorithm>
#include <execution>
#include <random>
#include "lib/DataLogger.hpp"

/* This defines the extremity of our antenna setup */
#define START_X 0.0
#define END_X 20.0
#define N_ANTENNAS (1<<6)

#define PI 3.1415926535

struct Vec2d
{
    double x = 0;
    double y = 0;
};

int main(int argc, char *argv[])
{
    
    /* Retrieve the source location from arguments */
    if (argc < 3)
    {
        fprintf(stderr, "Not enough arguments. Usage:\n\t./dataGenerate <x_source> <y_source> [<noise>]\n");
        exit(EXIT_FAILURE);
    }
    Vec2d source = {std::atof(argv[1]), std::atof(argv[2])};
    if (source.y <= 0)
    {
        fprintf(stderr, "Source's location should be on the upper half plane!\n");
        exit(EXIT_FAILURE);
    }

    double noise = (argc == 4) ? std::atof(argv[3]) : 0.05;
    std::random_device rd;
    std::mt19937 mt( rd() );
    std::uniform_real_distribution<double> unif(0, 2*PI);
    std::normal_distribution<double> normal(0, std::sqrt(noise));


    /* Compute the distance from each antennas and add a random offset and noise to it */
    std::vector<double> distances(N_ANTENNAS);
    int i = 0;
    for (auto& d : distances)
        d = ((double)i++ / (double)(N_ANTENNAS-1)) * (double)(END_X - START_X) + START_X;

    double offset = unif( mt );
    std::transform( distances.begin(), distances.end(), distances.begin(),
            [&](const auto& x) -> auto {
                return -std::sqrt( (x - source.x)*(x -source.x) + source.y * source.y ) + offset + normal( mt );
            });

    /* Wrap everything to [0,2PI) */
    std::transform(std::execution::par_unseq,
            distances.begin(), distances.end(), distances.begin(),
            [](const auto& x) -> auto {
            return std::fmod( std::fmod( x , 2*PI ) + 2*PI, 2*PI);
            });

    /* Ensure the data directory exists */
    std::filesystem::create_directories( "data" );

    /* Generate the filename according to source location and noise value */
    std::string filename = "Source:" + std::to_string(source.x) + "," + std::to_string(source.y) +
        "Noise" + std::to_string(noise) + ".dat";
    logVecDoubleToFile("data/" + filename , distances);
    std::cout << "Generated and saved data with\n\tsource: (" << source.x << ", " << source.y
        << ")\tand noise: " << noise << "\nat data/" << filename << '\n';

    return 0;
}
