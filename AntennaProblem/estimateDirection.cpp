#include <iostream>
#include <sstream>
#include <filesystem>
#include <stdio.h>
#include <cstdlib>
#include <cmath>
#include <ctime>
#include <string>
#include <vector>
#include <variant>
#include <algorithm>
#include <execution>
#include <Eigen/Core>
#include <Eigen/Dense>


#include "lib/Graph.hpp"
#include "lib/Timer.hpp"
#include "lib/DataLogger.hpp"

#define MAX_ITER 1'000'000
#define ONE_OVER_TWOPI 0.15915494309189535
#define TWOPI 6.283185307179586

using Eigen::Matrix;
using Eigen::Dynamic;
typedef Matrix<double, 2, 1> LineParam;
typedef Matrix<double, 3, 1> HyperbolaParam;

#define CONVERGENCE_TOL 1e-5
#define SINGULARITY_TOL 1e-3

/* Some functions definitions */
std::variant<LineParam, HyperbolaParam> GetSourceLocation(const int& start_x, const int& end_x,
        const std::vector<double>& unwrapped_phase, const int& max_it = 500, const double& lr = 0.1);
void computeJac(Matrix<double, Dynamic, 3>& jacobian, Matrix<double, 3, 1>& params, const Matrix<double, Dynamic, 2>& data);
void computeRes(Matrix<double, Dynamic, 1>& residues, Matrix<double, 3, 1>& params, const Matrix<double, Dynamic, 2>& data);
Matrix<double, 2, 1> linearRegression(const Matrix<double, Dynamic, 2>& data);

int main(int argc, char *argv[])
{

    if (argc < 5)
    {
        fprintf(stderr, "Please provide enough arguments. Typical usage: $ ./run <sig_u> <sig_z> <sig_w> <sig_n> < DATAFILE\n");
        exit(EXIT_FAILURE);
    }
    // Load the data from cin
    std::cout << "Reading the data...\n";
    std::string input;
    std::getline(std::cin, input);
    std::stringstream ss(input);
    std::vector<double> data;
    for (std::string word; ss >> word;)
        data.emplace_back( std::atof(word.c_str()) );

    if (data.empty())
    {
        fprintf(stderr, "Please provide data via cin, i.e. $ ./run < DATA\n");
        exit(EXIT_FAILURE);
    }

    // Get the parameters from argv
    double sig_u = std::atof(argv[1]), sig_z = std::atof(argv[2]);
    double sig_w = std::atof(argv[3]), sig_n = std::atof(argv[4]);

    /* We have to scale data into [0,1) */
    std::transform( std::execution::par_unseq,
            data.begin(), data.end(), data.begin(),
            [](const auto& x) -> auto {
            return x * ONE_OVER_TWOPI;
            });

    /* Wrap data into [0,1] */
    /* Note that it shouldn't be necessary with how we generated data, still, for good measure */
    std::transform( std::execution::par_unseq,
            data.begin(), data.end(), data.begin(),
            [](const double& x) -> double {
            return std::fmod( std::fmod(x, 1.0) + 1.0, 1.0);
            });


    Graph g(data, sig_z, sig_u, sig_w, sig_n);
    // Fit the model
    std::cout << "Reconstructing the unwrapped phase...\n";
    {
        Timer t;
        g.Fit(MAX_ITER);
    }
    if (g.HasConverged())
        std::cout << "The algorithm has converged, i.e. all BNUV are binarized\n";
    else
        std::cout << "The algorithm hasn't converged, i.e. not all BNUV are binarized\n";

    // Extract the true phase estimate
    std::vector<double> phi;
    g.GetComponent(&phi, 0);

    /* We stretch back phi into [0,2pi)*/
    std::transform(std::execution::par_unseq,
            phi.begin(), phi.end(), phi.begin(),
            [](const auto& x) -> auto {
            return x * TWOPI;
            });

    /* Retreive the direction of the source */
    std::variant<LineParam, HyperbolaParam> results;
    results = GetSourceLocation(0, 20, phi);

    try
    {
        HyperbolaParam param = std::get<HyperbolaParam>(results);
        std::cout << "Source location estimate:\n\t" << param(0) << ", " << std::exp(param(1)) << '\n';
        std::cout << "Source's global direction:\n\t" << std::atan2(std::exp(param(1)), param(0)) << " rad\n";
    }
    catch (const std::bad_variant_access& ex)
    {
        LineParam param = std::get<LineParam>(results);
        std::cout << "Source direction estimate:\t" << std::acos(std::max(-1.0, std::min(param(0), 1.0))) << " rad\n";
    }

    /* Compose filename */
    const auto time = std::time(0);
    std::string dstName = "sig_u" + std::to_string(sig_u) + "-sig_z" + std::to_string(sig_z)
        + "-sig_n" + std::to_string(sig_n) + "-sig_w" + std::to_string(sig_w)
        + "-MaxIter" + std::to_string(MAX_ITER) + "---" + std::to_string(time) + "---";

    /* Make sure the folder exists */
    std::filesystem::create_directories( "data" );

    std::cout << "Writting reconstruction to file: data/" << dstName << "phi.csv\n";
    logVecDoubleToFile("data/" + dstName + "phi.csv" , phi);

    return 0;
}


/* This compute the residuals from given parameters and data */
void computeRes(Matrix<double, Dynamic, 1>& residues, Matrix<double, 3, 1>& params, const Matrix<double, Dynamic, 2>& data)
{
    /* Defines the objective func */
    auto func = [&params](const auto& x) -> auto {
        return  params(2) - std::sqrt( (params(0) - x)*(params(0) - x) + std::exp(2.0*params(1)) );
    };
    /* Compute each residues */
    for (int i = 0; i < (int)data.rows(); ++i)
        residues(i) = data(i, 1) - func( data(i, 0) );

}

/* Compute the jacobian from given parameters and data */
void computeJac(Matrix<double, Dynamic, 3>& jacobian, Matrix<double, 3, 1>& params, const Matrix<double, Dynamic, 2>& data)
{
    auto d1 = [&params](const auto& x) -> auto {
        return (params(0) - x) / std::sqrt( (params(0) - x)*(params(0) - x) + std::exp(2.0*params(1)) );
    };
    auto d2 = [&params](const auto& x) -> auto {
        return std::exp(2.0*params(1))  / std::sqrt( (params(0) - x)*(params(0) - x) + std::exp(2.0*params(1)) );
    };
    /* auto d3 = [](const auto&) -> auto { */
    /*     return 1; */
    /* }; */

    for (int i = 0; i < (int)data.rows(); ++i)
    {
        jacobian(i, 0) = d1(data(i, 0));
        jacobian(i, 1) = d2(data(i, 0));
        jacobian(i, 2) = -1.0;
    }

}

/* Compute the source's location and returns either a the hyperbola parameters or if the data is close to a */
/* line, returns the linear regression parameters */
std::variant<LineParam, HyperbolaParam> GetSourceLocation(const int& start_x, const int& end_x,
        const std::vector<double>& unwrapped_phase, const int& max_it, const double& lr)
{
    /* Put the data into appropriate matrices */
    Matrix<double, Dynamic, 2> data(unwrapped_phase.size(), 2);
    for (int i = 0; i < (int)unwrapped_phase.size(); ++i)
    {
        data(i, 1) = unwrapped_phase[i];
        data(i, 0) = (double)(i) / (double)(unwrapped_phase.size()-1) * (double)(end_x - start_x);
    }

    /* The initial parameters */
    Matrix<double, 3, 1> lambda = {1,1,1};
    /* The residuals */
    Matrix<double, Dynamic, 1> R(unwrapped_phase.size(), 1);
    /* The Jacobian */
    Matrix<double, Dynamic, 3> J(unwrapped_phase.size(), 3);

    /* Loop until convergence or max_it is attained */
    for (int i = 0; i < max_it; ++i)
    {
        computeRes(R, lambda, data);
        computeJac(J, lambda, data);
        /* The internal algorithms of Eigen are quite stable. I had to increase the tolerence of the singularity */
        /*     detector quite a bit. You can decrease it a lot if you don't really care about the source precise location */
        /*     and care more about retreiving the approximate direction only */
        if ((J.transpose() * J).determinant() <= SINGULARITY_TOL)
        {
            std::cout << "Data close to line! Switching to linear regression\n";
            Matrix<double, 2, 1> params = linearRegression(data);
            return params;
        }
        Matrix<double, 3, 1> A = (J.transpose() * J).ldlt().solve(J.transpose() * R);
        lambda -= lr * A;
        if (A.squaredNorm() <= CONVERGENCE_TOL)
        {
            std::cout << "Least square fitting converged after " << i << " iterations\n";
            return lambda;
        }
    }
    return lambda;
}


/* Very simple linear regression algorithm */
Matrix<double, 2, 1> linearRegression(const Matrix<double, Dynamic, 2>& data)
{
    Matrix<double, Dynamic, 2> A(data.rows(), 2);
    A.col(0) = data.col(0);
    for (auto& x : A.col(1))
        x = 1;
   return (A.transpose() * A).ldlt().solve(A.transpose() * data.col(1));
}
