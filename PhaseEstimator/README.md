# Generate Data
the script `generateData.cpp` lets you generate data according to your preferred distribution.
It saves the results in a `<unix-time>.dat` file.

# Compute the Estimates
The file `computeEstimate.cpp` creates a Graph instance and compute the estimate for various parameters $\sigma_Z^2$ depending on the data it is fed.
Note that this uses `std::cin` to read data, this lets us simply feed it toy data by

        $ echo 0.1 0.9 | tr ' ' '\n' | ./computeEstimate

Otherwise you have to put data separated by newlines---as the `generateData.cpp` script already does---and feed it to the algorithm as

        $ ./computeEstimate < data.dat


# Plotting
The plotting script used is `PhaseEstimator.py`.
Data consist of 64 points generated around 0.85, with a variance of 0.07.
![Phase Estimation](Figure.png "Figure")
