#include <fstream>
#include <sstream>
#include <iostream>
#include <vector>
#include <random>

#include "lib/Graph.hpp"
#include "lib/Timer.hpp"
#include "lib/DataLogger.hpp"

#define MAX_ITER 1'000'000

#define START_SIG_Z 10
#define END_SIG_Z 100
#define INCR_SIG_Z 0.02

// Usefull data to track when testing
struct  Result{
    double mean = 0.0;
    int iterations = 0;
    bool converged = false;
};

// Take data, sig_z, and simulate the algorithm and give the pertinent result back into outRes
void simulate(const std::vector<double>& data, const double& sig_z, Result* outRes)
{
    Graph graph(data, sig_z);
    int i = 0;
    while ( i < MAX_ITER && !graph.HasConverged() )
    {
        graph.DataPropagation();
        graph.ForwardPropagation();
        graph.BackwardPropagation();
        graph.UpdateBin();

        ++i;
    }

    outRes->mean = graph.GetPrediction();
    outRes->iterations = i;
    outRes->converged = graph.HasConverged();
}

int main(int argc, char* argv[])
{

    std::string filename = (argc > 2) ? argv[1] : "output";

    std::vector<double> data;

    /* Load the data from cin */
    std::cout << "Loading data from std::cin...\n";
    std::string line;
    while(std::getline(std::cin, line))
    {
        if (line.empty())
        {
            fprintf(stderr, "Error, empty line. This uses cin as imput, e.g.:\n\
                    \t./computeEstimate [output_filename] < data.dat\n");
            exit(EXIT_FAILURE);
        }
        data.emplace_back( std::atof( line.c_str() ) );
    }

    /* Generate the range of sigma Z we test, we use a log scale from START_SIG_Z to END_SIG_Z*/
    double start_z = std::log(START_SIG_Z), end_z = std::log(END_SIG_Z);
    std::vector<double> increment_Z;
    for (double z = start_z; z <= end_z; z += INCR_SIG_Z)
        increment_Z.emplace_back(std::exp(z));

    /* Will store the results */
    std::vector<double> results;
    std::vector<int> iterations;
    results.reserve(increment_Z.size());
    iterations.reserve(increment_Z.size());

    /* The actual computations */
    std::cout << "Start computing the estimates...\n";
    {
        Timer t;
        Result res;
        for (auto sig_z : increment_Z)
        {
            simulate(data, sig_z, &res);
            results.emplace_back(res.mean);
            iterations.emplace_back(res.iterations);
        }
    }
    /* Log everything */
    std::cout << "Computation done. Writing results to file " << filename+".csv\n";
    logVecDoubleToFile(filename+".csv", data, increment_Z, results, std::vector<double>(iterations.begin(), iterations.end()));

    return 0;
}
