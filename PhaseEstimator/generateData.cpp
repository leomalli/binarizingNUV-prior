#include <fstream>
#include <vector>
#include <random>
#include <algorithm>
#include <execution>
#include <ctime>

int main(int argc, char* argv[])
{
    /* Get the arguments */
    if (argc < 4)
    {
        fprintf(stderr, "Usage:\n\t./genData <mean> <var> <nb_samples>\n");
        exit(EXIT_FAILURE);
    }
    double mu = std::atof(argv[1]), var = std::atof(argv[2]);
    int nbSamples = std::atoi(argv[3]);

    /* Instanciate the random devices */
    std::random_device rd;
    std::mt19937 mt( rd() );
    std::normal_distribution<double> dist(mu, std::sqrt(var));

    /* Generate the unwrapped data */
    std::vector<double> data;
    data.reserve(nbSamples);
    for (int i = 0; i < nbSamples; ++i)
        data.emplace_back( dist(mt) );

    /* Wrap it */
    std::transform(std::execution::par_unseq,
            data.begin(), data.end(),
            data.begin(), [](const double& x) -> double {
            return fmod( fmod( x, 1.0) + 1.0, 1.0 );
    });

    /* Write it to a file in a single column */
    std::ofstream file(std::to_string(std::time(0)) + ".dat", std::ios::out);
    std::copy(data.begin(), data.end(), std::ostream_iterator<double>(file, "\n"));

    return 0;
}
