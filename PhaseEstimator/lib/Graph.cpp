#include <vector>
#include <iostream>
#include <cmath>

#include "Graph.hpp"

#define VAR_TOL 1e-2

Graph::Graph(const std::vector<double>& data)
    : m_data(data), m_Z_var(4.1)
{ m_Initialize(); }

Graph::Graph(const std::vector<double>& data, const double& Z_var)
    : m_data(data), m_Z_var(Z_var)
{ m_Initialize(); }

Graph::~Graph()
{}

// Reserve the memory and initialize the priors
void Graph::m_Initialize()
{
    m_binPriors.reserve(m_data.size());
    m_dataMessages.reserve(m_data.size());

    m_forward.reserve(m_data.size() - 1);
    m_backward.reserve(m_data.size() - 1);

    // Populate the priors with initial values and dataMessages with empty msg
    for (int i = 0; i < (int)m_data.size(); ++i)
    {
        m_binPriors.emplace_back(
                std::array<BinPrior, 2>({BinPrior(1.0, 0.0), BinPrior(0.0, -1.0)})
                );
        m_dataMessages.emplace_back(0.0, 0.0);
    }
    // Populate with empty messages
    for (int i = 0; i < (int)m_data.size() - 1; ++i)
    {
        m_forward.emplace_back(0.0,0.0);
        m_backward.emplace_back(0.0,0.0);
    }
}

// Get the messages from data points
void Graph::DataPropagation()
{
    for (std::size_t i = 0; i < m_dataMessages.size(); ++i)
    {
        // Get the messages from both binPriors
        Msg s_plus, s_minus;
        m_binPriors[i][0].outMsg( &s_plus );
        m_binPriors[i][1].outMsg( &s_minus );

        // Add both messages according to respective rules
        Msg combined_bin = s_plus + s_minus;
        combined_bin.mean *= -1.0;


       m_dataMessages[i] = Msg( m_data[i] , m_Z_var ) + combined_bin;
    }
}

// Run through the graph and populate the m_forward messages
//          
//          This assumes that the data has already been propagated
void Graph::ForwardPropagation()
{
    m_forward[0] = m_dataMessages[0];
    for (std::size_t idx = 1; idx < m_forward.size(); ++idx)
    {
        // Computing msg passing trough equality node
        Msg tmp(0, 0);
        tmp.mean = m_forward[idx - 1].mean * m_dataMessages[idx].var + m_dataMessages[idx].mean * m_forward[idx - 1].var;
        tmp.mean /= (m_forward[idx - 1].var + m_dataMessages[idx].var);

        tmp.var = m_forward[idx - 1].var * m_dataMessages[idx].var / (m_forward[idx - 1].var + m_dataMessages[idx].var);

        m_forward[idx] = tmp;
    }
}

// Run through the graph backward and propagate messages
//          
//          This assumes that the data has already been propagated
void Graph::BackwardPropagation()
{

    m_backward.back() = m_dataMessages.back();
    for (std::size_t idx = m_backward.size() - 1; idx > 0; --idx)
    {
        // Computing msg passing trough equality node
        Msg tmp(0, 0);
        tmp.mean = m_backward[idx].mean * m_dataMessages[idx].var + m_dataMessages[idx].mean * m_backward[idx].var;
        tmp.mean /= (m_backward[idx].var + m_dataMessages[idx].var);

        tmp.var = m_backward[idx].var * m_dataMessages[idx].var / (m_backward[idx].var + m_dataMessages[idx].var);

        m_backward[idx - 1] = tmp;
    }
}

// Will take the informations in the messages and updates the binarizors accordingly
void Graph::UpdateBin()
{
    // Loop the vector containing the priors
    for (std::size_t idx = 0; idx < m_binPriors.size(); ++idx)
    {
        // Compute V_S+- using (>W_S + <W_S)^-1

        // First compute and store both messages from the priors
        Msg prior_plus, prior_minus;
        m_binPriors[idx][0].outMsg(&prior_plus);
        m_binPriors[idx][1].outMsg(&prior_minus);

        // Get the message comming up from stem
        double up_var = m_GetUpVar(idx);
        double up_mean = m_GetUpMean(idx);

        // Compute posteriori var
        // if prior_var == 0, this must be zero
        double post_plus_V, post_minus_V;

        // Compute for the + bin
        double tmp_incoming_var = m_Z_var + prior_minus.var + up_var;
        post_plus_V = prior_plus.var * tmp_incoming_var / (prior_plus.var + tmp_incoming_var);

        // Compute for the - bin
        tmp_incoming_var = m_Z_var + prior_plus.var + up_var;
        post_minus_V = prior_minus.var * tmp_incoming_var / (prior_minus.var + tmp_incoming_var);

        // Compute the respectives posterior means
        double post_plus_M, post_minus_M;
        post_plus_M = (m_Z_var + prior_minus.var + up_var) * prior_plus.mean + prior_plus.var * (m_data[idx] - up_mean - prior_minus.mean);
        post_plus_M /= (prior_minus.var + tmp_incoming_var);
        post_minus_M = (m_Z_var + prior_plus.var + up_var) * prior_minus.mean + prior_minus.var * (m_data[idx] - up_mean - prior_plus.mean);
        post_minus_M /= (prior_minus.var + tmp_incoming_var);


        m_binPriors[idx][0].UpdateParams(post_plus_M, post_plus_V);
        m_binPriors[idx][1].UpdateParams(post_minus_M, post_minus_V);
    }
}



double Graph::m_GetUpVar(const std::size_t& idx) const
{
    if (idx == 0)
    {
        return m_backward[idx].var;
    }
    else if (idx == m_forward.size())
    {
        return m_forward[idx - 1].var;
    }
    else
    {
        return m_forward[idx - 1].var * m_backward[idx].var / (m_forward[idx - 1].var + m_backward[idx].var);
    }
}

double Graph::m_GetUpMean(const std::size_t& idx) const
{
    if (idx == 0)
    {
        return m_backward[idx].mean;
    }
    else if (idx == m_forward.size())
    {
        return m_forward[idx - 1].mean;
    }
    else
    {
        return (m_forward[idx - 1].mean * m_backward[idx].var + m_backward[idx].mean * m_forward[idx - 1].var) / (m_forward[idx - 1].var + m_backward[idx].var);
    }
}

// Check is all the BNUV have converged to something or not
bool Graph::HasConverged() const
{
    double cum_var = 0.0;
    for (const auto& b : m_binPriors)
    {
        cum_var += b[0].GetVar() + b[1].GetVar();
    }

    return (cum_var <= VAR_TOL);

}

double Graph::GetPrediction() const
{
    // m_X = V_X (\fwd{W_X}\fwd{m_X} + \bwd{W_X}\bwd{m_X})
    // V_X = (\fwd{W_X} + \bwd{W_X})^{-1}

    // Computing V_X
    double V = m_forward.back().var * m_backward.back().var / ( m_forward.back().var + m_backward.back().var );

    // Computing m_X
    double M = m_forward.back().mean / m_forward.back().var + m_backward.back().mean / m_backward.back().var;
    M *= V;

    // Get the value back into the range [0, 1]
    return std::fmod( M + 1.0, 1.0 );
}
