#include <iostream>

#include "Msg.hpp"

Msg::Msg()
    : mean(0.0), var(0.0)
{}
Msg::Msg(const double& a, const double& b)
    : mean(a), var(b)
{}
Msg::~Msg()
{}

void Msg::Print() const
{
    std::cout << "Mean: " << mean << " Var: " << var << '\n';
}

Msg Msg::operator+(const Msg& other)
{ return Msg(mean+other.mean, var+other.var); }

Msg Msg::operator-(const Msg& other)
{ return Msg(mean-other.mean, var-other.var); }
