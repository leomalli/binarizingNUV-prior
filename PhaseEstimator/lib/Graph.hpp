#pragma once
#include <vector>
#include <array>

#include "BinPrior.hpp"

class Graph
{
public:
    Graph(const std::vector<double>& data);
    Graph(const std::vector<double>& data, const double& Z_var);
    ~Graph();

    void UpdateBin();

    void DataPropagation();
    void ForwardPropagation();
    void BackwardPropagation();

    [[nodiscard]] double GetPrediction() const;
    [[nodiscard]] bool HasConverged() const;

private:
    void m_Initialize();
    [[nodiscard]] double m_GetUpVar(const std::size_t& idx) const;
    [[nodiscard]] double m_GetUpMean(const std::size_t& idx) const;

private:
    // Contains the datapoints
    std::vector<double> m_data;
    // Size of the var of Z
    double m_Z_var;
    // Contains the messages:
    //      dataInformation are messages from data, forward and backward are X_k's themself in the main branch.
    std::vector<Msg> m_dataMessages, m_forward, m_backward;

    // Contains all the priors in array [S+, S-]
    std::vector<std::array<BinPrior, 2>> m_binPriors;
};
