#include <random>
#include "BinPrior.hpp"

#define EPS 1e-3

BinPrior::BinPrior(const double& a, const double& b)
    : m_a(a), m_b(b), m_sig_a(1.0), m_sig_b(1.0)
{
    static std::random_device rd;
    static std::mt19937 mt( rd() );
    static std::normal_distribution<double> dist( 1.0, 0.05 );

    // Random initialisation
    m_sig_a = dist( mt );
    m_sig_b = dist( mt );
    m_sig_a = (m_sig_a <= EPS) ? 1.0 : m_sig_a;
    m_sig_b = (m_sig_b <= EPS) ? 1.0 : m_sig_b;
}
BinPrior::~BinPrior()
{ }


void BinPrior::outMsg(Msg* const out_msg) const
{
    if ( m_sig_a > EPS && m_sig_b > EPS )
    {
        double sum_var = m_sig_a + m_sig_b;
        double v_out = m_sig_a * m_sig_b / sum_var;
        double m_out = m_b * m_sig_a + m_a * m_sig_b;
        m_out /= sum_var;
        out_msg->mean = m_out;
        out_msg->var = v_out;
    }
    else
    {
        out_msg->var = 0.0;
        out_msg->mean = (m_sig_a <= EPS) ? m_a : m_b;
    }

}

double BinPrior::GetVar() const
{
    if ( m_sig_a <= EPS || m_sig_b <= EPS ) return 0.0;

    return m_sig_a * m_sig_b / (m_sig_a + m_sig_b);
}

void BinPrior::UpdateParams(const double& p_mean, const double& p_var)
{
    m_sig_a = p_var + ( p_mean - m_a ) * ( p_mean - m_a );
    m_sig_b = p_var + ( p_mean - m_b ) * ( p_mean - m_b );
}

