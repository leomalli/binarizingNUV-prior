#pragma once
/* The only aim of this file is to provide a simple logging interface */

#include <fstream>
#include <vector>

/* The vectory passed should all have the same underlying datatype */
void logVecDoubleToFile(const std::string& filename, const auto&... args)
{
    std::fstream file(filename, std::ios::out);
    for (const auto& vec : {args...})
    {
        for (auto s{vec.size()}; auto elem : vec)
        {
            file << elem << (--s ? ' ' : '\n');
        }
    }
    file.close();
}
