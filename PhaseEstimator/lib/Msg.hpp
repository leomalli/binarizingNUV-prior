#pragma once

class Msg
{
public:
    Msg();
    Msg(const double& mean, const double& var);
    ~Msg();

    void Print() const;

    Msg operator+(const Msg& other);
    Msg operator-(const Msg& other);

double mean, var;
};
