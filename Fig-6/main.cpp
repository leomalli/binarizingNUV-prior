#include <fstream>
 
#define N_ITERATION 10000
#define INCREMENT 0.001
#define A_MU 0.0
#define B_MU 1.0


#define MU_INF -0.7
#define MU_SUP 1.7
 
double X_EM(double mu, double s_squared);

int main()
{
    // open file stream
    std::ofstream fout("data.csv", std::ios::out | std::ios::trunc);

    // First dump the increments into the file
    for (double mu = MU_INF ; mu < MU_SUP ; mu += INCREMENT)
        fout << mu << ' ';
    fout << '\n';

    // Compute values for some s^2
    for (double mu = MU_INF ; mu < MU_SUP ; mu += INCREMENT)
        fout << X_EM(mu, 0.00) << ' ';
    fout << '\n';
    for (double mu = MU_INF ; mu < MU_SUP ; mu += INCREMENT)
        fout << X_EM(mu, 0.225) << ' ';
    fout << '\n';
    for (double mu = MU_INF ; mu < MU_SUP ; mu += INCREMENT)
        fout << X_EM(mu, 5.0) << ' ';

    fout.close();

    return 0;
}


double X_EM(double mu, double s_squared)
{
    // Initialise some variables
    double sig_a_squared = 1.0, sig_b_squared = 1.0, V_X, X_k;

    // Loop through the data and update our estimates
    for (int i = 0; i < N_ITERATION; ++i)
    {
        double sum = (sig_a_squared * sig_b_squared + sig_b_squared * s_squared + s_squared*sig_a_squared);
        V_X = (sig_a_squared * sig_b_squared * s_squared) / sum;

        X_k = A_MU * sig_b_squared * s_squared + B_MU * sig_a_squared * s_squared + mu * sig_a_squared * sig_b_squared;
        X_k /= sum;

        sig_a_squared = V_X + (X_k - A_MU) * (X_k - A_MU);
        sig_b_squared = V_X + (X_k - B_MU) * (X_k - B_MU);
    }
    return X_k;
}
