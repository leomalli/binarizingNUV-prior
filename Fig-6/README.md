# Goal
Reproducing the binarizing effect of a BNUV prior.

# Plotting
The corresponding script is `GenerateFig6.py`

![Resulting Figure](Figure.png "Figure 6")
