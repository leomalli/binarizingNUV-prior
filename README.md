# Goal of the Project
Explore the use of Binarizing Normal with Unknown Variance (BNUV) priors in the unwrapping of a noisy wrapped signal.

# Organisation of the Code

### Fig-6
Familiarization with the framework by reproducing the binarizing effect of the priors. [^bnuvpaper]

### PhaseEstimator
We explore a model who's goal is to estimate the true phase of a noisy wrapped constant signal.

### First and Second Model
These models use BNUV priors to unwrap a noisy phase measurement.
In order to run them you must have [Eigen](https://eigen.tuxfamily.org/index.php?title=Main_Page) somewhere on your computer.
If Eigen is located on usual linker path you can just run `make`, otherwise you have to modify the make file and add Eigen's path to the flags.

### AntennaProblem
This aims to apply the above models to a simple real-world problem, retreiving the source of a signal, given its noisy phase measurements.
It is a quite simple and direct application and we merely add some linear algebra to the previous scripts in order to fit the unwrapped phase estimate to a certain family of hyperbola.

### PlotsScripts
Some python script used to plot the results of the different algorithms.
You can run them either as bash scripts `$ ./script.py` or as python script `$ python3 script.py`.

# Running the code
You should have some basic tools in order to compile the code, i.e. `make`, `g++`, and for plotting, `python3` with `matplotlib` and `numpy`.
Note that you can change the compiler to your preferred one in the respective Makefiles, just make sure it is compatible with at least the C++20 standard.
Compiling the code should be as easy as `cd` into the correct folder and executing `make release`---or just `make` for the debug build.
I have not tested the workflow in other environments than Debian bookworm on the following kernel's version: `5.18.0-4-amd64`.



# References

[^bnuvpaper]: R. Keusch, H.-A. Loeliger, ``A Binarizing NUV Prior and its Use for M-Level Control and Digital-to-Analog Conversion'', May 2021; Direct link: [https://arxiv.org/pdf/2105.02599.pdf](https://arxiv.org/pdf/2105.02599.pdf).
